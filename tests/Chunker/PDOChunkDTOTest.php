<?php
/**
 * Created by PhpStorm.
 * User: aecohen
 * Date: 26/10/17
 * Time: 6:26 PM
 */

use PHPUnit\Framework\TestCase;
use DataChunker\Chunker\PDOChunkDTO;

class PDOChunkDTOTest extends TestCase
{

    /** @var PDOChunkDTO */
    private $dto;

    /** @var \Mockery\MockInterface */
    private $stmt;

    protected function setUp()
    {
        parent::setUp();
        $this->stmt = Mockery::mock(PDOStatement::class);
        $this->dto = new PDOChunkDTO($this->stmt);
    }

    /**
     * @param array $data
     * @dataProvider dtoDataProvider
     */
    public function testGetAll($data){
        $this->stmt->shouldReceive('fetchAll')->andReturn($data);
        $this->assertEmpty(array_diff($data, $this->dto->getAll()));
    }

    /**
     * @param array $data
     * @dataProvider dtoDataProvider
     */
    public function testGetNext($data){
        $this->stmt->shouldReceive('fetch')->andReturn($data);
        $res = $this->dto->getNext();
        $this->assertEmpty(array_diff($data, $res));
    }

    /**
     * @param array $data
     * @dataProvider dtoDataProvider
     */
    public function testGetChunkSize($data)
    {
        $this->stmt->shouldReceive('rowCount')->andReturn(count($data));
        $this->assertEquals(count($data), $this->dto->getChunkSize());
    }

    public function dtoDataProvider(){
        return [
            [[]],
            [['a' => 'b', 'c' => 'd']],
            [['a' => 'b', 'c' => 'd', 'e' => 'f']],
        ];
    }

    public function testGetHeader(){
        $headers = ['Id', 'Name', 'Other Headers'];
        $dto = new PDOChunkDTO($this->stmt, $headers);
        $result = $dto->getHeaders();
        $this->assertEmpty(array_diff($headers, $result));
    }

}
