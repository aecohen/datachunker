<?php

use PHPUnit\Framework\TestCase;
use DataChunker\Chunker\PDOChunker;
use DataChunker\Utility\Result;

/**
 * Created by PhpStorm.
 * User: aecohen
 * Date: 22/10/17
 * Time: 12:26 PM
 */

class PdoChunkerTest extends TestCase{

    /** @var \Mockery\MockInterface */
    private $dumper;

    /** @var \Mockery\MockInterface */
    private $pdo;

    /** @var PDOChunker */
    private $chunker;

    protected function setUp(){
        parent::setUp();
        $this->dumper = Mockery::mock(\DataChunker\Dumper\IDumper::class);
        $this->pdo = Mockery::mock(PDO::class);
        $this->chunker = new PDOChunker($this->pdo, $this->dumper);
    }

    protected function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    /**
     * Test the process functionality without extra params
     *
     * @param string $query
     * @param array $rowCount
     * @param array $dumpReturn
     * @param bool $expectedResult
     * @param int $expectedCode
     * @dataProvider processProvider
     */
    public function testProcessNoParams($query, $rowCount = [], $dumpReturn = [true, 0], $expectedResult = true, $expectedCode = 0){
        $stmt = Mockery::mock(PDOStatement::class);
        $stmt->shouldReceive('bindValue')->andReturn(true); //don't care the arguments in this test
        $stmt->shouldReceive('execute')->andReturn(true); //don't care the arguments in this test
        $stmt->shouldReceive('rowCount')->andReturn(...$rowCount); //don't care the arguments in this test

        $this->pdo->shouldReceive('prepare')->withArgs(function($arg) use ($query){
            if(stristr($arg, $query) !== false){
                return true;
            }
            return false;
        })->andReturns($stmt);

        //We process the dump without problem
        $this->dumper->shouldReceive('dump')->andReturn(new Result($dumpReturn[0],'','',$dumpReturn[1]));


        //Set the Fail Safe small to be able to test it
        $this->chunker->setFailSafe(15);
        //Set the chunk size to be small as we don't need too many repetitions on this test
        $result = $this->chunker->process($query, [], 5);
        $this->assertEquals($expectedResult, $result->bResult);
        if($expectedCode !== '') {
            $this->assertEquals($expectedCode, $result->iCode);
        }
    }

    public function processProvider(){
        return [
          ['Random Wrong Query', [], [true, 0], false, 2],     //Test wrong query
          ['SELECT * FROM nothing', [], [true, 0], false, 2],  //Test Query without ORDER BY
          ['SELECT * FROM nothing ORDER BY something', [1,1,0,0], [false, 100], false, 100],  //Test Dump failed scenario. Any return code
          ['SELECT * FROM nothing ORDER BY something', [1,1,0,0]],  //Test Basic Success Scenario
          ['SELECT * FROM nothing ORDER BY something', [5,5,5,5,0,0]],  //Test Success Scenario with couple of dumps
          ['SELECT * FROM nothing ORDER BY something', [5,5,5,5,5,5], [true, 0], false, 1],    //Test Exceed Fail Safe
        ];
    }

    /**
     * Test the process functionality
     *
     * @param string $query
     * @param array $arguments
     * @param array $rowCount
     * @param bool $expectedResult
     * @param int $expectedCode
     * @dataProvider processWithParamProvider
     */
    public function testProcessWithParams($query, $arguments = [], $rowCount = [], $expectedResult = true, $expectedCode = 0){
        $stmt = Mockery::mock(PDOStatement::class);
        $stmt->shouldReceive('bindValue')->withArgs(function(...$args) use ($arguments) {
            $result = false;
            if(isset($arguments['mode']) && $arguments['mode'] == PDOChunker::QUESTION_MODE) {
                $originalKeys = [count($arguments['values']) + 1, count($arguments['values']) + 2];
            }else{
                $originalKeys = [':limit', ":offset"];
            }

            if(isset($arguments['values'])){
                $validKeys = array_merge($originalKeys, array_keys($arguments['values']));
                $validValues = array_values($arguments['values']);
            }

            if(in_array($args[0],$validKeys) && (in_array($args[1],$validValues))){
                $result = true;
            }else if(in_array($args[0],$validKeys) && is_int($args[1])){
                $result = true;
            }

            return $result;
        })->andReturn(true); //don't care the arguments in this test
        $stmt->shouldReceive('execute')->andReturn(true); //don't care the arguments in this test
        $stmt->shouldReceive('rowCount')->andReturn(...$rowCount); //don't care the arguments in this test


        $this->pdo->shouldReceive('prepare')->withArgs(function($arg) use ($query){
            if(stristr($arg, $query) !== false){
                return true;
            }
            return false;
        })->andReturns($stmt);

        //We process the dump without problem
        $this->dumper->shouldReceive('dump')->andReturn(new Result());


        //Set the Fail Safe small to be able to test it
        $this->chunker->setFailSafe(15);
        //Set the chunk size to be small as we don't need too many repetitions on this test
        $result = $this->chunker->process($query, $arguments, 5);
        $this->assertEquals($expectedResult, $result->bResult);
        if($expectedCode !== '') {
            $this->assertEquals($expectedCode, $result->iCode);
        }
    }

    public function processWithParamProvider(){
        return [
            ['SELECT * FROM nothing ORDER BY something', ['values' => [':some' => 'value']], [1,1,0,0]],  //Don't care about the query itself
            ['SELECT * FROM nothing ORDER BY something', ['values' => [':some' => 'value']], [5,5,5,5,0,0]],  //Don't care about the query itself
            ['SELECT * FROM nothing ORDER BY something', ['values' => [':some' => 'value']], [5,5,5,5,5,5], false, 1],    //Don't care about the query itself
            ['SELECT * FROM nothing ORDER BY something', ['mode' => PDOChunker::QUESTION_MODE, 'values' => ['1' => 'value']], [1,1,0,0]],  //Don't care about the query itself
            ['SELECT * FROM nothing ORDER BY something', ['mode' => PDOChunker::QUESTION_MODE, 'values' => ['1' => 'value']], [5,5,5,5,0,0]],  //Don't care about the query itself
            ['SELECT * FROM nothing ORDER BY something', ['mode' => PDOChunker::QUESTION_MODE, 'values' => ['1' => 'value']], [5,5,5,5,5,5], false, 1],    //Don't care about the query itself

        ];
    }
}