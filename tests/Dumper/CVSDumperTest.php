<?php

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use org\bovigo\vfs\vfsStreamWrapper;
use PHPUnit\Framework\TestCase;
use DataChunker\Chunker\PDOChunkDTO;
use DataChunker\Dumper\CSVDumper;
use DataChunker\Dumper\IDumper;

/**
 * Created by PhpStorm.
 * User: aecohen
 * Date: 23/10/17
 * Time: 11:29 PM
 */

class CVSDumperTest extends TestCase
{

    /** @var IDumper */
    private $dumper;

    protected function setUp()
    {
        parent::setUp();
        vfsStreamWrapper::register();
        vfsStreamWrapper::setRoot(new vfsStreamDirectory('testDir'));
        $this->dumper = new CSVDumper(vfsStream::url('testDir/test.csv'));
    }

    /**
     * Test Basic success scenarios
     *
     * @param array $data
     * @dataProvider basicScenarioProvider
     */
    public function testSimpleCase($data){
        $content = Mockery::mock(PDOChunkDTO::class);
        $content->shouldReceive('getNext')->andReturn(...$data);
        $content->shouldReceive('getHeaders')->andReturn(false);
        $result = $this->dumper->dump($content);
        $this->assertTrue($result->bResult);
        $this->assertTrue(vfsStreamWrapper::getRoot()->hasChild('testDir/test.csv'));
        $file = vfsStreamWrapper::getRoot()->getChild('testDir/test.csv');
        foreach ($data as $row) {
            if($row !== false) {
                foreach ($row as $value) {
                    $this->assertContains($value, $file->getContent());
                }
            }
        }
    }

    public function basicScenarioProvider(){
        return [
            [[['key1' => 'value1','key2' => 'value2'],['key1' => 'value3','key2' => 'value4'], false]]
        ];
    }

    /**
     * Test Failure Scenarios. Uses very similar code to the basic test
     *
     * @param array $data
     * @dataProvider basicScenarioProvider
     */
    public function testFailScenario($data){
        // an empty array means an empty folder
        $structure = [
            'csv' => []
        ];

        $root = vfsStream::setup('root',null,$structure);
        $dumper = new CSVDumper($root->url().'/testDir/test.csv');

        $content = Mockery::mock(PDOChunkDTO::class);
        $content->shouldReceive('getNext')->andReturn(...$data);
        vfsStream::newFile('testDir/test.csv', null);
        $result = $dumper->dump($content);
        $this->assertFalse($result->bResult);
        $this->assertFalse(vfsStreamWrapper::getRoot()->hasChild('testDir/test.csv'));
    }

}