<?php
/**
 * This interfaces provides the required method for a dumper to be able to transfer the queried data
 *
 * User: aecohen
 * Date: 17/10/17
 * Time: 11:24 PM
 */

namespace DataChunker\Dumper;

use DataChunker\Chunker\IChunkDTO;
use DataChunker\Utility\Result;

interface IDumper {

    /**
     * Proceed to transfer the chunk of data into the instantiated dumper (CSV File, Output, etc)
     *
     * @param IChunkDTO $content
     * @return Result
     */
    public function dump(IChunkDTO $content);

}