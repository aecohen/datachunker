<?php
/**
 * Implementation of the IDumper for CSV file creation. Specifically to be stored in the filesystem
 *
 * User: aecohen
 * Date: 17/10/17
 * Time: 11:24 PM
 */

namespace DataChunker\Dumper;

use DataChunker\Chunker\IChunkDTO;
use DataChunker\Utility\Result;

class CSVDumper implements IDumper{

    /** @var string */
    private $filePath;

    /** @var string */
    private $mode;

    /** @var resource|bool */
    private $fp;

    public function __construct($filePath, $overwrite = true){
        $this->filePath = $filePath;
        $this->mode = ($overwrite ? 'w+' : 'a+');
    }

    public function dump(IChunkDTO $content){
        $result = new Result();

        if(!isset($this->fp)){
            //Leave the opening of the file when we actually want to dump data
            $this->fp = @fopen($this->filePath, $this->mode);
            if($this->fp === false) {
                $result->bResult = false;
                $result->sMessage = 'Problem opening the file for writing';
            }else{
                $headers = $content->getHeaders();
                if($headers !== false){
                    $result = $this->putData($headers);
                }
            }
        }

        if($result->bResult) {
            while ($row = $content->getNext()) {
                if(is_array($row)) {
                    $result = $this->putData($row);
                    if($result->bResult === false){
                        break;
                    }
                }
            }
        }

        return $result;
    }

    private function putData($data){
        $result = new Result();
        $res = fputcsv($this->fp, $data);

        if($res === false){
            $result->bResult = false;
            $result->sMessage = 'Problem writing the data';
        }

        return $result;
    }
}