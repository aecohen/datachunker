<?php
/**
 * Created by PhpStorm.
 * User: aecohen
 * Date: 17/10/17
 * Time: 11:46 PM
 */

namespace DataChunker\Chunker;


abstract class BaseChunkDTO implements IChunkDTO{

    /** @var string */
    protected $type;

    /** @var array */
    protected $headers;

    public function __construct($headers)
    {
        $this->headers = $headers;
    }

    public function getType(){
        return $this->type;
    }

    public function getHeaders(){
        return (isset($this->headers) ? $this->headers : false);
    }
}