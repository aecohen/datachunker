<?php
/**
 * Kind of DTO to be used during the transfer of the information to a Dumper.
 * The idea is to be able to mask the source of the data from the Dumper
 *
 * Created by PhpStorm.
 * User: aecohen
 * Date: 17/10/17
 * Time: 11:44 PM
 */

namespace DataChunker\Chunker;


interface IChunkDTO {

    /**
     * Retrieves all the entries of the current chunk.
     * This method probably defeats the purpose of the chunk system, however it might prove useful.
     *
     * @return array|false Of arrays with associative values related with one entry for the requested query. False on failure
     */
    public function getAll();

    /**
     * Retrieves the next entry of the current chunk
     *
     * @return array|false Of associative values related with one entry for the requested query. False if there are no more entries
     */
    public function getNext();

    /**
     * Returns the size of the current chunk
     *
     * @return int|bool  Size of the current chunk. False if not defined
     */
    public function getChunkSize();

    /**
     * Returns the type of the current instantiated ChunkDTO
     *
     * @return string
     */
    public function getType();

    /**
     * Usually most of the data been transferred has some kind of headers. If none exist it returns false
     *
     * @return array|false A 1 dimension array with the headers for the data provided. False if none set
     */
    public function getHeaders();

}