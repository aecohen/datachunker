<?php
/**
 * Created by PhpStorm.
 * User: aecohen
 * Date: 10/10/17
 * Time: 12:24 AM
 */

namespace DataChunker\Chunker;

use DataChunker\Utility\Result;

interface IChunker {

    const CODE_ERROR_FAIL_SAFE = 1;
    const DEFAULT_CHUNK_SIZE = 5000;
    /** DEFAULT FAIL SAFE IN CASE THE SCRIPT MIGHT RUN FOREVER */
    const FAIL_SAFE_MAX_COUNT = 500000;


    /**
     * Set the fail safe limit for when processing a queryElement. This prevent infinity loops.
     * Default value is 500000
     *
     * @param int   $amount
     * @return void
     */
    public function setFailSafe($amount);

    /**
     * Process the desire queryElement into chunks using the extra parameters if needed
     *
     * @param mixed $queryElement
     * @param array $extraParams
     * @param int   $chunkSize
     * @return Result
     */
    public function process($queryElement, $extraParams = [], $chunkSize = self::DEFAULT_CHUNK_SIZE);

}