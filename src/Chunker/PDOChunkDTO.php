<?php
/**
 * Implementation of the IChunkDTO for PDO Statements
 *
 * User: aecohen
 * Date: 17/10/17
 * Time: 11:34 PM
 */

namespace DataChunker\Chunker;


class PDOChunkDTO extends BaseChunkDTO{

    protected $type = 'PDO';

    /** @var \PDOStatement */
    protected $statement;

    public function __construct(\PDOStatement $statement = null, $headers = []){
        $this->statement = $statement;
        parent::__construct($headers);
    }

    /**
     * Setter for the statement
     *
     * @param \PDOStatement $statement
     */
    public function setStatement(\PDOStatement $statement){
        $this->statement = $statement;
    }

    public function getAll(){
        return (isset($this->statement) ? $this->statement->fetchAll(\PDO::FETCH_ASSOC) : false);
    }

    public function getNext(){
        return (isset($this->statement) ? $this->statement->fetch(\PDO::FETCH_ASSOC) : false);
    }

    public function getChunkSize()
    {
        return (isset($this->statement) ? $this->statement->rowCount() : false);
    }


}