<?php
/**
 * Created by PhpStorm.
 * User: aecohen
 * Date: 10/10/17
 * Time: 12:37 AM
 */

namespace DataChunker\Chunker;

use PDO;
use DataChunker\Dumper\IDumper;
use DataChunker\Utility\Result;

class PDOChunker implements IChunker{

    const CODE_ERROR_PREPARE = 2;

    /** @var \PDO */
    private $pdo;

    /** @var IDumper */
    private $dumper;

    /** @var int Upper limit to stop possible infinite loop */
    private $failSafe;

    const QUESTION_MODE = 1;
    const PLACEHOLDER_MODE = 2;

    public function __construct(\PDO $pdo, IDumper $dumper){
        $this->pdo = $pdo;
        $this->dumper = $dumper;
        $this->failSafe = self::FAIL_SAFE_MAX_COUNT;
    }

    public function setFailSafe($amount){
        $this->failSafe = $amount;
    }

    public function process($queryElement, $extraParams = [], $chunkSize = self::DEFAULT_CHUNK_SIZE){

        $result = new Result();
        $headers = (isset($extraParams['headers']) ? $extraParams['headers'] : null);
        $pdoDTO = new PDOChunkDTO(null, $headers);
        $offset= 0;
        list($stmt, $extraParams) = $this->prepareStatement($queryElement, $extraParams);

        if($stmt === false){
            $result->bResult = false;
            $result->iCode = self::CODE_ERROR_PREPARE;
            $result->sMessage = 'Problem preparing the statement';
            return $result;
        }

        //Initial query to start the process
        $stmt = $this->queryStatement($stmt, $extraParams, $chunkSize, $offset);
        $total = $stmt->rowCount();

        for($offset = $chunkSize; $stmt->rowCount() > 0; $offset += $chunkSize){

            $pdoDTO->setStatement($stmt);
            $res = $this->dumper->dump($pdoDTO);
            if($res->bResult === false){
                $result = $res;
                return $result;
            }

            if($total >= $this->failSafe){
                $result->bResult = false;
                $result->iCode = self::CODE_ERROR_FAIL_SAFE;
                $result->sMessage = 'Query exceeded the fail safe limit. You might try raising the limit. Part of the dump was made';
                return $result;
            }
            $stmt = $this->queryStatement($stmt, $extraParams, $chunkSize, $offset);
            $total += $stmt->rowCount();
        }

        $result->mData = $total;

        return $result;
    }

    private function queryStatement(\PDOStatement $stmt, $extraParams, $limit, $offset){

        $limit = intval($limit);
        $offset = intval($offset);
        $counter = 1;
        $totalParams = 0;

        if(isset($extraParams['values'])) {
            $totalParams = (isset($extraParams['values']) ? count($extraParams['values']) : 0);
            foreach ($extraParams['values'] as $key => $value) {
                if ($extraParams['mode'] == self::PLACEHOLDER_MODE) {
                    $stmt->bindValue($key, $value);
                } else {
                    $stmt->bindValue($counter, $value);
                    $counter++;
                }

            }
        }

        if($extraParams['mode'] == self::PLACEHOLDER_MODE){
            $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
            $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        }else{
            $stmt->bindValue($totalParams+1, $limit, PDO::PARAM_INT);
            $stmt->bindValue($totalParams+2, $offset, PDO::PARAM_INT);
        }

        $stmt->execute();

        return $stmt;
    }

    private function prepareStatement($query, $extraParams){
        if(stristr($query, 'ORDER BY') === false){
            return [false, $extraParams];
        }

        if(!isset($extraParams['mode'])){
            $extraParams['mode'] = self::PLACEHOLDER_MODE;
        }

        switch($extraParams['mode']){
            case self::QUESTION_MODE:
                $query .= ' LIMIT ? OFFSET ?';
                break;
            case self::PLACEHOLDER_MODE:
            default:
                $query .= ' LIMIT :limit OFFSET :offset';
                break;
        }


        return [$this->pdo->prepare($query), $extraParams];
    }

}