<?php
/**
 * Simple DTO type result
 *
 * User: aecohen
 * Date: 20/10/17
 * Time: 12:11 AM
 */

namespace DataChunker\Utility;


class Result {

    /** @var bool */
    public $bResult;

    /** @var mixed */
    public $mData;

    /** @var string */
    public $sMessage;

    /** @var int */
    public  $iCode;

    /** Sucess Code */
    const CODE_SUCCESS = 0;

    /**
     * @param bool $result
     * @param string $message
     * @param mixed $data
     * @param int $code
     */
    public function __construct($result = true, $message = '', $data = [], $code = self::CODE_SUCCESS){
        $this->bResult = $result;
        $this->mData = $data;
        $this->sMessage = $message;
        $this->iCode = $code;

    }
}